use std::sync::{
    mpsc::{channel, Receiver, Sender},
    Arc, Mutex,
};
use std::thread::JoinHandle;

pub enum ChefMessage {
    FoodRequest,
    AllServed,
}

struct Cook;

impl Cook {
    fn spawn(cooking_pot: Arc<Mutex<u32>>, order_channel: Receiver<ChefMessage>) -> JoinHandle<()> {
        std::thread::spawn(move || {
            // Wait on the channel for orc request
            loop {
                match order_channel.recv() {
                    Ok(ChefMessage::FoodRequest) => {
                        Cook::handle_food_request(&cooking_pot)
                    }
                    Ok(ChefMessage::AllServed) => {
                        break;
                    }
                    Err(err) => {
                        panic!("Could not read from the order channel: {}", err);
                    }
                }
            }
        })
    }

    fn handle_food_request(pot: &Arc<Mutex<u32>>) {
        let mut unlocked_pot = pot.lock().unwrap();
        if *unlocked_pot == 0 {
            println!("Preparing another pot");
            *unlocked_pot = 13;
        }
    }
}

struct Orc;

impl Orc {
    fn spawn(
        number: u32,
        cooking_pot: Arc<Mutex<u32>>,
        order_channel: Sender<ChefMessage>,
    ) -> JoinHandle<()> {
        std::thread::spawn(move || {
            loop {
                let mut unlocked_pot = cooking_pot.lock().unwrap();
                if *unlocked_pot > 0 {
                    *unlocked_pot -= 1;
                    break;
                } else {
                    order_channel.send(ChefMessage::FoodRequest).unwrap();
                }
            }
            println!("Orc {} is done eating", number);
        })
    }
}

fn main() {
    let cooking_pot = Arc::new(Mutex::new(0u32));
    let (order_tx, order_rx) = channel();

    let mut thread_handles: Vec<JoinHandle<()>> = vec![];
    let cook_handle = Cook::spawn(Arc::clone(&cooking_pot), order_rx);

    for orc in 0..19 {
        thread_handles.push(Orc::spawn(orc, Arc::clone(&cooking_pot), order_tx.clone()));
    }

    for handle in thread_handles {
        let _ = handle.join();
    }

    println!("All orcs served");
    order_tx.send(ChefMessage::AllServed).unwrap();
    let _ = cook_handle.join();

    let lock = cooking_pot.lock().unwrap();
    println!("{} servings remaining", *lock);
}
